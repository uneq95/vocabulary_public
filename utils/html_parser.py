import requests
from bs4 import BeautifulSoup

meaning = {}


def parse(word):
    url = "http://www.vocabulary.com/dictionary/" + word
    meaning["word"] = word
    # url = "http://www.google.com"
    src_binary = requests.get(url)
    soup = BeautifulSoup(src_binary.content, "html.parser")
    htmlFile = open("src_html.html", 'w')
    htmlFile.write(str(soup))
    htmlFile.close()
    soup = BeautifulSoup(open("src_html.html"), 'html.parser')
    # try:
    short_def_tag = soup.find("p", {"class": "short"}).contents
    short_def = ''
    for phrase in short_def_tag:
        short_def = short_def + unicode(phrase.string)
    # remove italics tags
    short_def = short_def.replace("<i>", "")
    short_def = short_def.replace("</i>", "")
    print "short def: "
    print short_def
    # except AttributeError:
    # short_def = "Not Found"
    # try:
    long_def_tag = soup.find("p", {"class": "long"}).contents
    long_def = ''
    for phrase in long_def_tag:
        long_def = long_def + unicode(phrase.string)
    # remove italics tags
    long_def = long_def.replace("<i>", "")
    long_def = long_def.replace("</i>", "")
    print "long def: "
    print long_def
    # except AttributeError:
    # long_def="Not Found"
    meaning["short_def"] = short_def
    meaning["long_def"] = long_def
    return meaning
