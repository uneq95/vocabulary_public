from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Meaning(models.Model):
    word = models.CharField(max_length=30)
    short_def = models.TextField()
    long_def = models.TextField()

